#include <stdio.h>
#include <stdlib.h>
#include "librarian.h"
#include"list.h"

void add_new_book()
{
	book_t book;
	int fd;

	fd = open("books.db", O_WRONLY | O_CREAT | O_APPEND, 0644);
	if(fd < 0)
	{
		perror("failed to open books file");
		_exit(1);
	}

	// Take information of the book to be added into library
	printf("Add book information : \n");

	printf("Enter Book ID : ");
	scanf("%d", &book.book_id);

	printf("\nEnter Subject : ");
	scanf("%*c%s", book.subject);
	printf("\nBook Name : ");
	scanf("%*c%s", book.bookName);
	printf("\nBook Author : ");
	scanf("%*c%s", book.authorName);
	printf("\nEnter Price of Book : ");
	scanf("%f", &book.price);
	printf("\nEnter ISBN : ");
	scanf("%d", &book.isbn);
	printf("\nEnter number of copies being added : ");
	scanf("%d", &book.book_count);

	printf("\n");

	int bytes = write(fd, &book, sizeof(book));
	printf("%d\n", bytes);
	close(fd);

	printf("%s Book has been successfully added.\n", book.bookName);
}

void add_new_copies(int id)
{
	copy_t copy;
	int fd;
	copy.book_count = 0;
	fd = open("copies", O_WRONLY | O_CREAT | O_APPEND, 0644);
	copy.book_id = id;
	printf("Enter rack : ");
	scanf("%d", &copy.rack);
	printf("Pre-copies = %d\n", copy.book_count);
	copy.book_count++;
	printf("Number of copies now is : %d\n", copy.book_count);
	write(fd, &copy, sizeof(copy));
	close(fd);
}

void issue_book_copy()
{
    issue_t issue;
    copy_t copy,input_copy;
    int fd_payment,fd_copy,fd_issue;
    payments_t pay;
    int cnt,cnt_book;
    int user_found = 0;
    int book_found = 0;
    memset(&issue,0,sizeof(issue_t));
    fd_issue = open("issues",O_RDWR|O_CREAT,0644);
    fd_payment = open("payment",O_RDWR|O_CREAT,0644);
    fd_copy = open("copies",O_RDWR);
    printf("enter member id:");
    scanf("%d",&issue.memberid);
    while((cnt = read(fd_payment,&pay,sizeof(pay))) > 0)
    {
        if(issue.memberid == pay.userid)
        {
            if(pay.amount > 0)
            {
                printf("enter book id: ");
                scanf("%d",&pay.id);

                while ((cnt_book = read(fd_copy,&copy,sizeof(copy))) > 0)
                {
                    if(pay.id ==  copy.book_id)
                    {
                        --copy.book_count;
                        write(fd_issue,&issue,sizeof(issue));
                        lseek(fd_copy, -sizeof(copy_t), SEEK_END);
                        write(fd_copy,&copy,sizeof(copy));
                        book_found = 1;

                    }
                }
            }
            else
            {
                printf("\nuser fee is not paid\n");
                break;
            }
            user_found =1;

        }
    }

    if(book_found == 0)
    {
        printf("\ncopy not found\n");
        // break;
    }


    if(user_found == 0)
    {
        printf("\nuser id is not found\n");
    }
    else
    {
        printf("book issued\n");
    }

    close(fd_copy);
    close(fd_issue);
    close(fd_payment);
}

void return_book_copy()
{
}

void list_issued_books()
{
}

void edit_book(char *book_name)
{
}

void find_book_by_name()
{
}

void check_book_availability()
{
}

void list_books()
{
	book_t book;
	int fd, cnt;
	fd = open("books.db", O_RDONLY, 0644);
	if(fd < 0)
	{
		perror("Unable to open file.");
		_exit(1);
	}
	printf("ID\tAUTHOR\tBOOK_NAME\tSUBJECT\tPRICE\tISBN\n");
	while((cnt = read(fd, &book, sizeof(book))) > 0)
	{
		printf("%d\t%s\t%s\t%s\t%0.4f\t%d\n", book.book_id, book.authorName, book.bookName, book.subject, book.price, book.isbn);
	}
	close(fd);
}

void add_user(void)
{
	user_t dat;
	FILE *fd;
	fd=fopen("new.dat","ab");
	printf("Enter the Student name :");
	scanf("%s",dat.name);
	printf("Enter the Email ID :");
	scanf("%s",dat.email);
	printf("Enter the New UserName :");
	scanf("%s",dat.username);
	printf("Enter the New Password :");
	scanf("%d",&dat.password);
	fwrite(&dat,sizeof(user_t),1,fd);
	fclose(fd);
}
