#include <stdio.h>
#include <stdlib.h>
#include "librarian.h"

enum menu_options{ SIGN_OUT, EDIT_PROFILE, CHANGE_PASSWORD, FIND_BOOK_BY_NAME, CHECK_BOOK_AVAILABILITY, ADD_NEW_BOOK, ADD_NEW_COPY, ISSUE_BOOK_COPY, RETURN_BOOK_COPY, LIST_ISSUED_BOOKS, EDIT_BOOK, CHANGE_RACK, ADD_MEMBER, TAKE_PAYMENT, PAYMENT_HISTORY, LIST_BOOKS, LIST_USERS, EXIT };

int menu(void)
{
	int choice;
	
	printf("0. Sign Out\n");
	printf("1. Edit Profile\n");
	printf("2. Change Password\n");
	printf("3. Find book by name\n");
	printf("4. Check book availability\n");
	printf("5. Add new book\n");
	printf("6. Add new copy\n");
	printf("7. Issue book copy\n");
	printf("8. Return book copy\n");
	printf("9. List issued books\n");
	printf("10. Edit book\n");
	printf("11. Change rack\n");
	printf("12. Add member\n");
	printf("13. Take payment\n");
	printf("14. Payment history\n");
	printf("15. List books\n");
	printf("16. List users\n");
	printf("17. EXIT\n");
	
	printf("Enter Your Choice : ");
	scanf("%d", &choice);
	//printf("choice = %d\n", choice);

	return choice;
}

int main()
{
	int choice, id;
	while(1)
	{
		choice = menu();
		//printf("Your choice = %d\n", choice);
		switch(choice)
		{
			case SIGN_OUT:
					printf("Selected Choice : Sign out\n");
					break;
			case EDIT_PROFILE: 
					printf("Selected Choice : Edit Profile\n");
					break;
			case FIND_BOOK_BY_NAME:
					printf("Selected Choice : Find book by name\n");
					break;
			case CHECK_BOOK_AVAILABILITY:
					printf("Selected Choice : Check book availability\n");
					break;
			case ADD_NEW_BOOK:
					printf("Selected Choice : Add new book\n");
					add_new_book();
					break;
			case ADD_NEW_COPY:
					printf("Selected Choice : Add new copy\n");
					printf("Enter id : ");
					scanf("%d", &id);
					add_new_copies(id);
					break;
			case ISSUE_BOOK_COPY:
					issue_book_copy();
					printf("Selected Choice : Issued book copy\n");
					break;
			case RETURN_BOOK_COPY:
					printf("Selected Choice : Return book copy\n");
					break;
			case LIST_ISSUED_BOOKS:
					printf("Selected Choice : List issued books\n");
					list_issued_books();
					break;
			case EDIT_BOOK:
					printf("Selected Choice : Edit book\n");
					break;
			case CHANGE_RACK:
					printf("Selected Choice : Change rack\n");
					break;
			case ADD_MEMBER:
					printf("Selected Choice : Add member\n");
					break;
			case TAKE_PAYMENT:
					printf("Selected Choice : Take Payment\n");
					break;
			case PAYMENT_HISTORY:
					printf("Selected Choice : Payment history\n");
					break;
			case LIST_BOOKS:
					printf("Selected choice : List Books\n");
					list_books();
					break;
			case LIST_USERS:
					printf("Selected choice : List Users\n");
					list_users();
					break;
			case EXIT:
					printf(" Selected Choice : Exit\n");
					exit(0);
		}
	}
return 0;
}
