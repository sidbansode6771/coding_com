#ifndef __LIBRARIAN_H_
#define __LIBRARIAN_H_

#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#define MAX_BOOK_NAME	50
#define MAX_AUTHOR_NAME	50
#define SUBJECT			50

// declare files to be used
#define BOOK_FILE		"books.txt"

// structure declarations
typedef struct
{
	unsigned int book_id;				// book id
	char bookName[MAX_BOOK_NAME];		// book name
	char authorName[MAX_AUTHOR_NAME];	// author name
	char subject[SUBJECT];				// subject name
	float price;						// price of book
	int isbn;							// isbn of book
	int book_count;						// book count
	bool status;						// book availability status
	int rack;							// rack number
}book_t;

// copies struct
typedef struct
{
	unsigned int book_id;	// book_id
	int rack;				// rack number
	bool status;			// availability status
	int book_count;			// number of copies
}copy_t;

// issue_record sruct
typedef struct issue_record
{
	int id;
	int copyid;
	int memberid;
	int issue_date;
	int return_duedate;
	int return_date;
	int fine_amount;
}issue_t;

typedef struct users
{
	int id;
	char name[20];
	char email[20];
	int phone;
	char password[10];
	char role[20];
    	int book_issue;
}users_t;

// payments struct
typedef struct payments
{
	int id;
	int userid;
	int amount;
	char type[20];
	int transaction_time;
	int nextpayment_duedate;
}payments_t;


// function declarations
int menu(void);
void add_new_book();
//void add_new_book_copies(int id);
//void add_new_copies(char *book_name, int count);
void add_new_copies(int id);
void issue_book_copy();
void return_book_copy();
void list_issued_books();
void edit_book(char *book_name);
void find_book_by_name();
void check_book_availability();
void list_books();
void list_users();

#endif
